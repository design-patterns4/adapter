package homework;

import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;
import homework.orm.first.FirstOrm;
import homework.orm.first.FirstOrmAdapter;
import homework.orm.second.SecondOrm;
import homework.orm.second.SecondOrmAdapter;

import java.time.LocalDateTime;

/**
 * Пример использования паттерна адаптер.
 *
 * @author Tatyana_Dolnikova
 */
public class App {

    public static void main(String[] args) {
        DbUserEntity user = new DbUserEntity(1L, "login", "password", 33L);
        DbUserInfoEntity info = new DbUserInfoEntity(33L, "name", LocalDateTime.now());

        FirstOrmAdapter firstOrmAdapter = new FirstOrmAdapter(new FirstOrm());
        firstOrmAdapter.createUser(user);
        firstOrmAdapter.createUserInfo(info);
        firstOrmAdapter.readUser(user.getId());
        firstOrmAdapter.readUserInfo(info.getId());
        firstOrmAdapter.updateUser(user);
        firstOrmAdapter.updateUserInfo(info);
        firstOrmAdapter.deleteUser(user);
        firstOrmAdapter.deleteUserInfo(info);

        SecondOrmAdapter secondOrmAdapter = new SecondOrmAdapter(new SecondOrm());
        secondOrmAdapter.createUser(user);
        secondOrmAdapter.createUserInfo(info);
        secondOrmAdapter.readUser(user.getId());
        secondOrmAdapter.readUserInfo(info.getId());
        secondOrmAdapter.updateUser(user);
        secondOrmAdapter.updateUserInfo(info);
        secondOrmAdapter.deleteUser(user);
        secondOrmAdapter.deleteUserInfo(info);
    }

}
