package homework.orm.first;

import homework.orm.entity.IDbEntity;

/**
 * FirstOrm.
 *
 * @author Tatyana_Dolnikova
 */
public class FirstOrm implements IFirstOrm<IDbEntity> {

    @Override
    public void create(IDbEntity entity) {
        System.out.println("FirstOrm: создан объект " + entity);
    }

    @Override
    public IDbEntity read(int id) {
        return null;
    }

    @Override
    public void update(IDbEntity entity) {

    }

    @Override
    public void delete(IDbEntity entity) {
        System.out.println("FirstOrm: удалён объект " + entity);
    }
}
