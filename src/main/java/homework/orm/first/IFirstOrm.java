package homework.orm.first;

import homework.orm.TargetOrm;
import homework.orm.entity.IDbEntity;

/**
 * IFirstOrm.
 *
 * @author Tatyana_Dolnikova
 */
public interface IFirstOrm<T extends IDbEntity> extends TargetOrm {

    void create(T entity);

    T read(int id);

    void update(T entity);

    void delete(T entity);
}