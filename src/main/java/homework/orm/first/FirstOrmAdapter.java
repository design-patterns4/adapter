package homework.orm.first;

import homework.orm.Orm;
import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;
import homework.orm.entity.IDbEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * Адаптер сервиса IFirstOrm.
 *
 * @author Tatyana_Dolnikova
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FirstOrmAdapter implements Orm {

    IFirstOrm<IDbEntity> firstOrm;

    public FirstOrmAdapter(IFirstOrm<IDbEntity> firstOrm) {
        this.firstOrm = firstOrm;
    }

    @Override
    public void createUser(DbUserEntity entity) {
        firstOrm.create(entity);
    }

    @Override
    public void createUserInfo(DbUserInfoEntity entity) {
        firstOrm.create(entity);
    }

    @Override
    public DbUserEntity readUser(Long id) {
        IDbEntity entity = firstOrm.read(id.intValue());
        return entity instanceof DbUserEntity ? (DbUserEntity) entity : null;
    }

    @Override
    public DbUserInfoEntity readUserInfo(Long id) {
        IDbEntity entity = firstOrm.read(id.intValue());
        return entity instanceof DbUserInfoEntity ? (DbUserInfoEntity) entity : null;
    }

    @Override
    public void updateUser(DbUserEntity entity) {
        firstOrm.update(entity);
    }

    @Override
    public void updateUserInfo(DbUserInfoEntity entity) {
        firstOrm.update(entity);
    }

    @Override
    public void deleteUser(DbUserEntity entity) {
        firstOrm.delete(entity);
    }

    @Override
    public void deleteUserInfo(DbUserInfoEntity entity) {
        firstOrm.delete(entity);
    }
}
