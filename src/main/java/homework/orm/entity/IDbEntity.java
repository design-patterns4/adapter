package homework.orm.entity;

/**
 * IDbEntity.
 *
 * @author Tatyana_Dolnikova
 */
public interface IDbEntity {

    Long getId();

    void setId(Long id);
}