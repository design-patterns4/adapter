package homework.orm.second;

import homework.orm.TargetOrm;
import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;

import java.util.Set;

/**
 * ISecondOrmContext.
 *
 * @author Tatyana_Dolnikova
 */
public interface ISecondOrmContext extends TargetOrm {
    Set<DbUserEntity> getUsers();

    Set<DbUserInfoEntity> getUserInfos();
}