package homework.orm.second;

import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * SecondOrmContext.
 *
 * @author Tatyana_Dolnikova
 */
public class SecondOrmContext implements ISecondOrmContext {

    @Override
    public Set<DbUserEntity> getUsers() {
        return new HashSet<>();
    }

    @Override
    public Set<DbUserInfoEntity> getUserInfos() {
        return new HashSet<>();
    }
}
