package homework.orm.second;

/**
 * ISecondOrm.
 *
 * @author Tatyana_Dolnikova
 */
public interface ISecondOrm {
    ISecondOrmContext getContext();
}
