package homework.orm.second;

/**
 * SecondOrm.
 *
 * @author Tatyana_Dolnikova
 */
public class SecondOrm implements ISecondOrm {

    @Override
    public ISecondOrmContext getContext() {
        return new SecondOrmContext();
    }
}
