package homework.orm.second;

import homework.orm.Orm;
import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

/**
 * Адаптер сервиса ISecondOrmContext.
 *
 * @author Tatyana_Dolnikova
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SecondOrmAdapter implements Orm {

    ISecondOrmContext secondOrmContext;

    public SecondOrmAdapter(ISecondOrm secondOrm) {
        secondOrmContext = secondOrm.getContext();
    }

    @Override
    public void createUser(DbUserEntity entity) {
        secondOrmContext.getUsers().add(entity);
    }

    @Override
    public void createUserInfo(DbUserInfoEntity entity) {
        secondOrmContext.getUserInfos().add(entity);
    }

    @Override
    public DbUserEntity readUser(Long id) {
        return secondOrmContext.getUsers().stream()
                .filter(u -> u.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    @Override
    public DbUserInfoEntity readUserInfo(Long id) {
        return secondOrmContext.getUserInfos().stream()
                .filter(u -> u.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    @Override
    public void updateUser(DbUserEntity entity) {
        boolean removed = secondOrmContext.getUsers().removeIf(u -> u.getId().equals(entity.getId()));
        if (removed) {
            secondOrmContext.getUsers().add(entity);
        }
    }

    @Override
    public void updateUserInfo(DbUserInfoEntity entity) {
        boolean removed = secondOrmContext.getUserInfos().removeIf(u -> u.getId().equals(entity.getId()));
        if (removed) {
            secondOrmContext.getUserInfos().add(entity);
        }
    }

    @Override
    public void deleteUser(DbUserEntity entity) {
        secondOrmContext.getUsers().remove(entity);
    }

    @Override
    public void deleteUserInfo(DbUserInfoEntity entity) {
        secondOrmContext.getUserInfos().remove(entity);
    }
}
