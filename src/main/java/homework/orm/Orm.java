package homework.orm;

import homework.orm.entity.DbUserEntity;
import homework.orm.entity.DbUserInfoEntity;

/**
 * Orm.
 *
 * @author Tatyana_Dolnikova
 */
public interface Orm {

    void createUser(DbUserEntity entity);

    void createUserInfo(DbUserInfoEntity entity);

    DbUserEntity readUser(Long id);

    DbUserInfoEntity readUserInfo(Long id);

    void updateUser(DbUserEntity entity);

    void updateUserInfo(DbUserInfoEntity entity);

    void deleteUser(DbUserEntity entity);

    void deleteUserInfo(DbUserInfoEntity entity);

}
